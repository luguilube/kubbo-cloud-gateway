FROM openjdk:11

VOLUME /tmp

EXPOSE 8090

ADD ./target/kubbo-cloud-gateway-0.0.1-SNAPSHOT.jar kubbo-cloud-gateway.jar

ENTRYPOINT ["java", "-jar", "/kubbo-cloud-gateway.jar"]