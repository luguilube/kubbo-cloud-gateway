## Kubbo Test App

Kubbo Test App es una app basada en una idea inicial de la empresa Kubbo, específicamente como prueba técnica para postulación a una vacante. Esta app fue diseñada en un principio por un conjunto de 6 microservicios para el backend  (servidor, customers, products, warehouses, orders, cloud gateway) para gestionar específicamente lo referente a cada funcionalidad requerida por la App, pero segmentada en módulos; y para el frontend un proyecto únicamente. Actualmente se sigue actualizando que nuevas funcionalidades y tecnologías para ser mostrada como proyecto de portafolio.

## Descripción del Proyecto

Toda la descripción detallada del proyecto, se encuentra dentro del repositorio de [kubbo-test-app-frontend](https://bitbucket.org/luguilube/kubbo-test-app-frontend/src/master/) 

## Kubbo Cloud Gateway

Microservicio que actúa como gateway y balanceador de carga, implementando Spring Cloud Gateway que asu ves implementa Spring Cloud Loadbalancer y Spring Cloud Netflix Eureka Client, esto le permite conectarse al microservicio Eureka Server y gestionar todas las peticiones HTTP dentro del proyecto de forma reactiva, y no encolando las mismas.

Cada 30 segundos actualiza el estado de los diferentes microservicios a los cuales se encuentra escuchando, y detecta de todas las instancias de cada uno las mejores en tiempos de respuesta para redireccionar las peticiones HTTP que le realicen a un determinado endpoint.

## Nota

Este debe ser ser el último microservicio en ser ejecutado ya que depende de la ejecución de los microservicios:

- kubbo-config-server
- kubbo-eureka-server
- kubbo-customer-microservice
- kubbo-users-microservice
- kubbo-product-microservice
- kubbo-warehouse-microservice
- kubbo-order-microservice