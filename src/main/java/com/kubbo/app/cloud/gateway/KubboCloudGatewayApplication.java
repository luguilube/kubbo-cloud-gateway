package com.kubbo.app.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class KubboCloudGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubboCloudGatewayApplication.class, args);
	}

}
